.. _about:

About
=====

Hi, I'm Marijn van der Zee. 
I'm a freelance software developer and basketball coach
with a love for performance analysis in basketball.

I've created `some scripts (in R) <https://github.com/serra/bball>`_
that use the publicly available box score statistics from the DBL
to calculate advanced performance indicators for both players and teams.
These performance indicators are available for :ref:`download here <downloads>`.
They are automatically updated once new data has become available,
usually several days after the game has been played.

You can contact me on `Github <http://www.github.com>`_ 
where I'm known as `serra <https://github.com/serra>`_.

If you'd like to hire me to do some software development or consultancy,
please refer to `www.serraict.com <http://www.serraict.com/contact>`_.
My professional expertise is in the field 
of automation and IT solutions in glasshouse horticulture,
but I'm interested in basketball related projects too.
